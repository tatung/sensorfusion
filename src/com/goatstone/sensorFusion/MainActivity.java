/************************************************************************************
 * Copyright (c) 2014 Jose Collas
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 ************************************************************************************/

/*
 * Test Source Tree
 */

package com.goatstone.sensorFusion;

import java.text.DecimalFormat;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.goatstone.util.SensorFusion;

public class MainActivity extends Activity implements SensorEventListener,
		RadioGroup.OnCheckedChangeListener {

	private SensorFusion sensorFusion;
	// private BubbleLevelCompass bubbleLevelCompass;
	private SensorManager sensorManager = null;

	// private RadioGroup setModeRadioGroup;
	private TextView azimuthText, pithText, rollText;
	private DecimalFormat d = new DecimalFormat("#.##");

	// ---the images to display---
	Integer[] imageIDs = { R.drawable.pic1, R.drawable.pic2, R.drawable.pic3,
			R.drawable.pic4, R.drawable.pic5, R.drawable.pic6, R.drawable.pic7,
			R.drawable.pic8 };

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		Gallery gallery = (Gallery) findViewById(R.id.galleryApps);

		gallery.setAdapter(new ImageAdapter(this));
		gallery.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView parent, View v, int position,
					long id) {
				Toast.makeText(getBaseContext(),
						"pic" + (position + 1) + " selected",
						Toast.LENGTH_SHORT).show();
			}
		});

		sensorManager = (SensorManager) this.getSystemService(SENSOR_SERVICE);
		registerSensorManagerListeners();

		d.setMaximumFractionDigits(0);
		d.setMinimumFractionDigits(0);

		sensorFusion = new SensorFusion();
		sensorFusion.setMode(SensorFusion.Mode.FUSION);

		// bubbleLevelCompass = (BubbleLevelCompass) this
		// .findViewById(R.id.SensorFusionView);
		// setModeRadioGroup = (RadioGroup) findViewById(R.id.radioGroup1);
		azimuthText = (TextView) findViewById(R.id.azmuth);
		pithText = (TextView) findViewById(R.id.pitch);
		rollText = (TextView) findViewById(R.id.roll);
		// setModeRadioGroup.setOnCheckedChangeListener(this);
	}

	public void updateOrientationDisplay() {

		double azimuthValue = sensorFusion.getAzimuth();
		double rollValue = sensorFusion.getRoll();
		double pitchValue = sensorFusion.getPitch();

		azimuthText.setText(String.valueOf(d.format(azimuthValue)));
		pithText.setText(String.valueOf(d.format(pitchValue)));
		rollText.setText(String.valueOf(d.format(rollValue)));

		// bubbleLevelCompass.setPLeft((int) rollValue);
		// bubbleLevelCompass.setPTop((int) pitchValue);
		// bubbleLevelCompass.setAzimuth((int) azimuthValue);

	}

	public void registerSensorManagerListeners() {
		sensorManager.registerListener(this,
				sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
				SensorManager.SENSOR_DELAY_FASTEST);

		sensorManager.registerListener(this,
				sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE),
				SensorManager.SENSOR_DELAY_FASTEST);

		sensorManager.registerListener(this,
				sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
				SensorManager.SENSOR_DELAY_FASTEST);
	}

	@Override
	public void onStop() {
		super.onStop();
		sensorManager.unregisterListener(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		sensorManager.unregisterListener(this);
	}

	@Override
	public void onResume() {
		super.onResume();
		registerSensorManagerListeners();
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		switch (event.sensor.getType()) {
		case Sensor.TYPE_ACCELEROMETER:
			sensorFusion.setAccel(event.values);
			sensorFusion.calculateAccMagOrientation();
			break;

		case Sensor.TYPE_GYROSCOPE:
			sensorFusion.gyroFunction(event);
			break;

		case Sensor.TYPE_MAGNETIC_FIELD:
			sensorFusion.setMagnet(event.values);
			break;
		}
		updateOrientationDisplay();
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {

		// switch (checkedId) {
		// case R.id.radio0:
		// sensorFusion.setMode(SensorFusion.Mode.ACC_MAG);
		// break;
		// case R.id.radio1:
		// sensorFusion.setMode(SensorFusion.Mode.GYRO);
		// break;
		// case R.id.radio2:
		// sensorFusion.setMode(SensorFusion.Mode.FUSION);
		// break;
		// }
	}

	public class ImageAdapter extends BaseAdapter {
		private Context context;
		private int itemBackground;

		public ImageAdapter(Context c) {
			context = c;
			// ---setting the style---
			TypedArray a = obtainStyledAttributes(R.styleable.Gallery1);
			itemBackground = a.getResourceId(
					R.styleable.Gallery1_android_galleryItemBackground, 0);
			a.recycle();
		}

		// ---returns the number of images---
		public int getCount() {
			return imageIDs.length;
		}

		// ---returns the ID of an item---
		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		// ---returns an ImageView view---
		public View getView(int position, View convertView, ViewGroup parent) {
			ImageView imageView = new ImageView(context);
			imageView.setImageResource(imageIDs[position]);
			imageView.setScaleType(ImageView.ScaleType.FIT_XY);
			imageView.setLayoutParams(new Gallery.LayoutParams(800, 600));
			imageView.setBackgroundResource(itemBackground);
			return imageView;
		}
	}

}